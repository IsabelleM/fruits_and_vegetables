import pandas as pd
from pandas_ods_reader import read_ods
import mysql.connector

connection = mysql.connector.connect(
host="localhost",       #l'url de la base de données
user="david",           #le nom d'utilisateur mysql
password="coucou123",   #le mot de passe de l'utilisateur mysql
database = "excel"      #le nom de la database
)

cursor = connection.cursor()

# Lire le fichier ods et le convertir en dataframe
df = pd.read_excel('pivot-tables.ods', engine='odf', sheet_name="Sheet1")

# Supprimer la première colonne index
df = df.set_index("Order ID")
print(df)


# for index, row in df.drop_duplicates().iterrows():
#     #Ajouter le pays
#     cursor.execute("INSERT INTO country (country_id, name) VALUES (NULL, %s);", (row['Country'],))
#     #Récupérer l'ID du restaurant
#     country_id = cursor.lastrowid
#     connection.commit()

#     #Ajouter le produit
#     cursor.execute("INSERT INTO product (product_id, name, category) VALUES (NULL, %s, %s);", (row['Product'], row['Category']))
#     product_id = cursor.lastrowid
#     connection.commit()

#     #Ajouter l'order
#     cursor.execute("INSERT INTO `order` (order_id, amount, date, product_id, country_id) VALUES (NULL, %s, %s, %s, %s)",
#                        (row['Amount'], row['Date'], product_id, country_id))
#     connection.commit()


for index, row in df.drop_duplicates().iterrows():
    # Ajouter le pays s'il n'existe pas déjà
    cursor.execute("SELECT country_id FROM country WHERE name = %s", (row['Country'],))
    existing_country = cursor.fetchone()
    if not existing_country:
        cursor.execute("INSERT INTO country (name) VALUES (%s)", (row['Country'],))
        country_id = cursor.lastrowid
    else:
        country_id = existing_country[0]

    # Ajouter le produit s'il n'existe pas déjà
    cursor.execute("SELECT product_id FROM product WHERE name = %s", (row['Product'],))
    existing_product = cursor.fetchone()
    if not existing_product:
        cursor.execute("INSERT INTO product (name, category) VALUES (%s, %s)", (row['Product'], row['Category']))
        product_id = cursor.lastrowid
    else:
        product_id = existing_product[0]

    # Ajouter la commande
    cursor.execute("INSERT INTO `order` (amount, date, product_id, country_id) VALUES (%s, %s, %s, %s)",
                   (row['Amount'], row['Date'], product_id, country_id))
    connection.commit()



cursor.close()
connection.close()
