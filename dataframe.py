import pandas as pd
import numpy as np
from main import df


# Regroupe les lignes par pays, les colonnes par produit et fait la somme du montant total vendu par pays 
#(correspond au tableau de la première feuille Excel, le two_dimmensional pivot_table)
df_pivot = pd.pivot_table(df, values = 'Amount',
                              index = ['Country'], 
                              columns = ['Product'],
                              aggfunc = [np.sum],
                              margins = True, margins_name = 'Total_results')
print(df_pivot)

df_pivot = pd.pivot_table(df, values = 'Amount',
                              index = ['Country'], 
                              columns = ['Product'],
                              aggfunc = [np.sum]).plot.barh(figsize=(10,7),
                              title='Montant total des produits par pays')

# Regroupe le nombre de commandes par commandes et par pays (correspond au deuxième tableau)
df_count = pd.pivot_table(df, values='Amount', index='Product', columns='Country',
               aggfunc='count', margins = True, margins_name = 'Total_results')
print(df_count)
