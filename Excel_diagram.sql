DROP DATABASE IF EXISTS excel;

CREATE DATABASE excel;

USE excel;

CREATE TABLE country (
  country_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(56)
);

CREATE TABLE product (
  product_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(56),
  category enum ('fruit','vegetables')
);

CREATE TABLE `order` (
  order_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  amount int,
  date date,
  product_id int,
  country_id int,
  FOREIGN KEY (product_id) REFERENCES product (product_id),
  FOREIGN KEY (country_id) REFERENCES country (country_id)
);

