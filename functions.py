import pandas as pd
from main import df
import mysql.connector


def get_product_orders_by_country(country_list):
    # Se connecter à la base de données
    connection = mysql.connector.connect(
        host="localhost",
        user="david",
        password="coucou123",
        database="excel"
    )
    cursor = connection.cursor(dictionary=True)

    # Initialiser le dictionnaire de résultats
    product_orders = {}

    # Pour chaque pays fourni
    for country in country_list:
        # Exécuter la requête pour récupérer les noms de produits et le nombre de commandes associées
        cursor.execute("""
            SELECT product.name AS product_name, COUNT(*) AS order_count
            FROM `order`
            INNER JOIN product ON `order`.product_id = product.product_id
            INNER JOIN country ON `order`.country_id = country.country_id
            WHERE country.name = %s
            GROUP BY product.name
        """, (country,))
        
        # Récupérer les résultats et les stocker dans le dictionnaire
        product_orders[country] = {row['product_name']: row['order_count'] for row in cursor}

    # Fermer la connexion à la base de données
    cursor.close()
    connection.close()

    return product_orders

# Exemple d'utilisation de la fonction
countries = ['France', 'Germany', 'United States']
result = get_product_orders_by_country(countries)
print(result)